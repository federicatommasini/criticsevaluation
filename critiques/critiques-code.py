class critique:
    def __init__(self, description, element=None):
        self.description = description # description of the problem related to the failed critique
        self.element = element # if present, element to which the failed critique is related to 

# function to define a new customized critique, it should have this signature!
# the element_list parameter is a list of treqs element to analyze when checking the critiques, they could be searched
# in all files of the repository or in a subset of them according to the option given to the command to run
# the ttim_types is the list of types that are specified in the Type and Trace Information model of the project
# parameters is a dictionary either taken by the configuration file where the critique is specified, or set by default here
def custom_critique(element_list, ttim_types,  parameters = None):

    #the success variable should be set to 0 when the check of the critique is successful 
    # for all elements in the element_list, while should be set to 1 when at least for one element the critique is violeted 
    # (note that the critique could be also not associated to a specific element but more general)
    success = 0

    # the critiques_found_list should be an empty list if the check is succesful
    # while should contain a list of "critique" objects to which a description of the problem is assigned and 
    # the eventual element to which is reffered is also assigned
    critiques_found_list = []

    return success, critiques_found_list
